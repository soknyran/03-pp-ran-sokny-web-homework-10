import React from "react";
import { Card, Button, Container } from "react-bootstrap";

function Profile({ user }) {
  return (
    <Container>
      <div className="mb-2">
        <Card>
          <Card.Img
            style={{ objectFit: "cover", height: "200px" }}
            variant="top"
            src={user.avatar}
          />
          <Card.Body>
            <Card.Title>{`${user.first_name} ${user.last_name}`}</Card.Title>
            <Card.Text>Email: {user.email}</Card.Text>
            <Button variant="primary">Go somewhere</Button>
          </Card.Body>
        </Card>
      </div>
    </Container>
  );
}

export default Profile;
