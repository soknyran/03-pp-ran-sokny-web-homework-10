import api from "../api/api";

export const fetchCategory = async () => {
  let response = await api.get("category");
  return response.data.data;
};

export const postCategory = async (category) => {
  let response = await api.post("category", category);
  return response.data.message;
};

export const fetchCategoryById = async (id) => {
  let response = await api.get("category/" + id);
  return response.data.data;
};

export const addCategory = async (category) => {
  let response = await api.post("category", category);
  return response.data.message;
};

export const deleteCategorybyId = async (id) => {
  let response = await api.delete("category/" + id);
  return response.data.message;
};

export const updateCategoryById = async (id, newCategory) => {
  let response = await api.put("category/" + id, newCategory);
  return response.data.message;
};
