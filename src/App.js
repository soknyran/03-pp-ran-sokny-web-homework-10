import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Menu from "./components/Menu";
import Article from "./page/Article";
import Category from "./page/Category";
import Author from "./page/Author";
import Post from "./page/Post";
import User from "./page/User";
import ViewArticle from "./page/ViewArticle";

function App() {
  return (
    <Router>
      <Menu />
      <Switch>
        <Route exact path="/" component={Article} />
        <Route path="/post" component={Post} />
        <Route path="/user" component={User} />
        <Route path="/category" component={Category} />
        <Route path="/author" component={Author} />
        <Route path="/article/:id" component={ViewArticle} />
        <Route path="/update/article/:id" component={Post} />
        <Route path="/*" render={() => <h1>404 Not Found</h1>} />
      </Switch>
    </Router>
  );
}

export default App;
