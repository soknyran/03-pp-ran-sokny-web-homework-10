import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row, Card } from "react-bootstrap";
import { useHistory } from "react-router";
import { fetchArticle, deleteArticle } from "../services/article_service";

function Article() {
  const [articles, setArticles] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const fetch = async () => {
      let articles = await fetchArticle();
      setArticles(articles);
    };
    fetch();
  }, []);

  const onDelete = (id) => {
    deleteArticle(id)
      .then((message) => {
        let newArticles = articles.filter((article) => article._id !== id);
        setArticles(newArticles);
        alert(message);
      })
      .catch((e) => console.log(e));
  };

  let articleCard = articles.map((article) => (
    <Col md={3} key={article._id}>
      <Card className="my-2">
        <Card.Img
          variant="top"
          style={{ objectFit: "cover", height: "150px" }}
          src={
            article.image
              ? article.image
              : "https://designshack.net/wp-content/uploads/placeholder-image.png"
          }
        />
        <Card.Body>
          <Card.Title>{article.title}</Card.Title>
          <Card.Text className="text-line-3">{article.description}</Card.Text>
          <Button
            variant="primary"
            onClick={() => history.push("/article/" + article._id)}
          >
            View
          </Button>{" "}
          <Button
            variant="warning"
            onClick={() => {
              history.push("/update/article/" + article._id);
            }}
          >
            Edit
          </Button>{" "}
          <Button variant="danger" onClick={() => onDelete(article._id)}>
            Delete
          </Button>
        </Card.Body>
      </Card>
    </Col>
  ));
  return (
    <Container>
      <Row>{articleCard}</Row>
    </Container>
  );
}

export default Article;
