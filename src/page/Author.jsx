import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import {
  deleteAuthor,
  fetchAuthor,
  postAuthor,
  updateAuthor,
} from "../services/author_service";
import { uploadImage } from "../services/article_service";

function Author() {
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [isUpdate, setIsUpdate] = useState(null);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    let author = await fetchAuthor();
    setAuthor(author);
  };

  const onAdd = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      newAuthor.image = url;
    }
    postAuthor(newAuthor).then((message) => {
      alert(message);
      fetch();
    });
    setImageFile(null);
    clear();
  };

  function clear() {
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("image").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  function onDelete(id) {
    deleteAuthor(id);
    let afterDelete = author.filter((item) => item._id !== id);
    setAuthor(afterDelete);
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id);
    setName(name);
    setEmail(email);
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      newAuthor.image = url;
    }
    updateAuthor(isUpdate, newAuthor).then(() => fetch());
    setIsUpdate(null);
    setImageFile(null);
    clear();
  };

  return (
    <div>
      <Container>
        <Row>
          <Col md={9}>
            <Form>
              <Form.Group>
                <Form.Label>Author Name</Form.Label>
                <Form.Control
                  id="name"
                  type="text"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>
              <Form.Group>
                <Form.Label>Emai</Form.Label>
                <Form.Control
                  id="email"
                  type="text"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? "Update" : "Add"}
              </Button>
            </Form>
          </Col>
          <Col md={3}>
            <Form.Label>Upload Photo</Form.Label>
            <img id="image" className="w-100" src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="image"
                  label="Choose Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {author.map((item, index) => (
              <tr key={index}>
                <td>{item._id.slice(0, 5)}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width={200} height={120} />
                </td>
                <td>
                  <Button
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    Edit
                  </Button>{" "}
                  <Button variant="danger" onClick={() => onDelete(item._id)}>
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}

export default Author;
