import React, { useState, useEffect } from "react";
import { Form, Button, Table, Container } from "react-bootstrap";
import { useHistory, useLocation } from "react-router";
import query from "query-string";
import {
  fetchCategory,
  fetchCategoryById,
  updateCategoryById,
  deleteCategorybyId,
  postCategory,
} from "../services/category_service";

function Category() {
  const [category, setCategory] = useState([]);
  const history = useHistory();
  const [name, setName] = useState("");
  const { search } = useLocation();
  console.log("Lacation:", search);
  let { id } = query.parse(search);
  console.log("id query:", id);

  useEffect(async () => {
    const result = await fetchCategory();
    console.log("category:", result);
    setCategory(result);
  }, []);

  useEffect(async () => {
    if (search == "") {
      setName("");
    } else {
      const result = await fetchCategoryById(id);
      setName(result.name);
    }
  }, [id]);

  const onAdd = async (e) => {
    e.preventDefault();
    let category = {
      name,
    };
    postCategory(category).then((message) => alert(message));
    clear();
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let category = {
      name,
    };
    updateCategoryById(id, category).then((message) => alert(message));
  };

  async function onDeleteCategoryById(id) {
    const result = await deleteCategorybyId(id);
    const temp = category.filter((item) => {
      return item._id != id;
    });
    setCategory(temp);
  }

  function clear() {
    document.getElementById("name").value = "";
  }

  return (
    <Container>
      <Form>
        <Form.Label>Category Name</Form.Label>
        <Form.Control
          placeholder="Category Name"
          style={{ width: "300px" }}
          id="name"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <Button
          variant="primary"
          onClick={id ? onUpdate : onAdd}
          className="my-3"
          variant="primary"
          type="button"
        >
          {id ? "Update" : "Add"}
        </Button>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>category</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {category.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item._id.slice(0, 5)}</td>
                <td>{item.name}</td>
                <td>
                  <Button
                    onClick={() => history.push(`/category?id=${item._id}`)}
                    variant="warning"
                    className="m-2"
                  >
                    Edit
                  </Button>
                  <Button
                    onClick={() => onDeleteCategoryById(item._id)}
                    variant="danger"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </Container>
  );
}

export default Category;
